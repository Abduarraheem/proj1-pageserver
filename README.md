# README

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

## Author: Abduarraheem Elfandi  
## Email: aelfandi@uoregon.edu  

  
The objectives of this mini-project are:

* Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
* Extend a tiny web server in Python, to check understanding of basic web architecture
* Use automated tests to check progress (plus manual tests for good measure)
  
* Test deployment in other environments. Deployment should work "out of the box" with this command sequence: 

  ```
  git clone <yourGitRepository> <targetDirectory>
  ```
  
  ```
  cd <targetDirectory>
  ```
    
Type this command to run the server.  
  ```
  make run or make start
  ```
  
  *test it with a browser now, while your server is running in a background process*
    
  To stop the server from running type this command in the command line. 

  ```
  make stop
  ```
